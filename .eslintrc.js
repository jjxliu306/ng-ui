module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/prettier"],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-unused-vars": "off", // 多余变量检查,声明了多余变量但没使用
    "no-irregular-whitespace": "off", //这禁止掉 空格报错检查
    "prettier/prettier": "off"//关闭prettier
  }
};

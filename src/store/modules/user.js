export default {
  namespaced: true,
  state: {
    id: "0",
    name: "",
    orgCode: "610000000000",
    deptId: ""
  },
  mutations: {
    updateId(state, id) {
      state.id = id;
    },
    updateName(state, name) {
      state.name = name;
    },
    updateOrgCode(state, orgCode) {
      state.orgCode = orgCode;
    },
    updateDeptId(state, deptId) {
      state.deptId = deptId;
    }
  }
};

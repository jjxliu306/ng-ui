import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import VueCookie from "vue-cookie"; // api: https://github.com/alfhen/vue-cookie
import "@/element-ui"; // api: https://github.com/ElemeFE/element
import "element-ui/lib/theme-chalk/index.css";

import "babel-polyfill";
import '@/assets/fonts/iconfont.css'

import "@/assets/scss/index.scss";
import httpRequest from "@/utils/httpRequest"; // api: https://github.com/axios/axios
import { isAuth, back } from "@/utils";
import cloneDeep from "lodash/cloneDeep";

import IconSvg from "@/components/icon-svg";
import sysDict from "@/components/dict/dict.vue";
import sysDictSelect from "@/components/dict/dictSelect.vue";
import sysDictRadio from "@/components/dict/dictRadio.vue";
import sysDictCheckbox from "@/components/dict/dictCheckbox.vue";
import user from  '@/components/user/index.vue'

import NgFormElement from 'ng-form-element'
import 'ng-form-element/lib/ng-form-element.css'

const altDictComponent = {
  install: function(Vue) {
    Vue.component("sysDict", sysDict);
  } // 'Loading'这就是后面可以使用的组件的名字，install是默认的一个方法
};
const altDictSelectComponent = {
  install: function(Vue) {
    Vue.component("sysDictSelect", sysDictSelect);
  } // 'Loading'这就是后面可以使用的组件的名字，install是默认的一个方法
};
const altDictRadioComponent = {
  install: function(Vue) {
    Vue.component("sysDictRadio", sysDictRadio);
  } // 'Loading'这就是后面可以使用的组件的名字，install是默认的一个方法
};
const altDictCheckboxComponent = {
  install: function(Vue) {
    Vue.component("sysDictCheckbox", sysDictCheckbox);
  } // 'Loading'这就是后面可以使用的组件的名字，install是默认的一个方法
};
const userComponent = {
  install: function (Vue) {
    Vue.component('user', user)
  } // 'Loading'这就是后面可以使用的组件的名字，install是默认的一个方法
}

Vue.component("IconSvg", IconSvg);
/* 数据字典翻译组件 */
Vue.use(altDictComponent);
Vue.use(altDictSelectComponent);
Vue.use(altDictRadioComponent);
Vue.use(altDictCheckboxComponent);
Vue.use(VueCookie);
Vue.use(userComponent)

Vue.use(NgFormElement) 

// 挂载全局
Vue.prototype.$http = httpRequest; // ajax请求方法
Vue.prototype.isAuth = isAuth; // 权限方法
Vue.prototype.back = back;

// 保存整站vuex本地储存初始状态
window.SITE_CONFIG["storeState"] = cloneDeep(store.state);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

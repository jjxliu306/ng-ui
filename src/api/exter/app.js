import http from '@/utils/httpRequest'


export function getList(params) {
  return http({
          url: http.adornUrl('/exter/app/list'),
          method: 'get',
          params: params
        })
} 
export function deleteApp(ids = []) {
	return http({
            url: http.adornUrl("/exter/app/delete"),
            method: "post",
            data: http.adornData(ids, false)
          })
}

export function getById(id) {
	return http({
        url: http.adornUrl(`/exter/app/info/${id}`),
        method: "get"
        })
}
 
export function saveOrUpdate(data) {
	return http({
            url: http.adornUrl(
              `/exter/app/${!data.id ? "save" : "update"}`
            ),
            method: "post",
            data: data
        })
}

export function updateAk(id) {
  return  http({
          url:  http.adornUrl(`/exter/app/updateAk/${id}`),
          method: "get",
          params:  http.adornParams()
        })
}
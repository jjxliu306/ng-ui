import http from '@/utils/httpRequest'

export function getList(params) {
  return http({
          url: http.adornUrl('/exter/log/list'),
          method: 'get',
          params: params
        })
} 
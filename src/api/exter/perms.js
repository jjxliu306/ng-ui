import http from '@/utils/httpRequest'

// 查询全部 进行选择
export function selectAll() {
  return http({
        url:  http.adornUrl(`/exter/perms/all`),
        method: "get",
        params:  http.adornParams()
      })
}

export function getList(params) {
  return http({
          url: http.adornUrl('/exter/perms/list'),
          method: 'get',
          params: params
        })
} 
export function deletePerm(ids = []) {
	return http({
            url: http.adornUrl("/exter/perms/delete"),
            method: "post",
            data: http.adornData(ids, false)
          })
}

export function getById(id) {
	return http({
        url: http.adornUrl(`/exter/perms/info/${id}`),
        method: "get"
        })
}
 
export function saveOrUpdate(data) {
	return http({
            url: http.adornUrl(
              `/exter/perms/${!data.id ? "save" : "update"}`
            ),
            method: "post",
            data: data
        })
}
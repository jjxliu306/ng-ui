import http from '@/utils/httpRequest'


export function getList(params) {
  return http({
          url: http.adornUrl('/form/data/list'),
          method: 'get',
          params: params
        })
}


export function getInfo(id) {
  return http.get(http.adornUrl(`/form/data/info/${id}`))
}


export function remove(ids) {
    return http({
          url: http.adornUrl('/form/data/delete'),
          method: 'post',
          data: http.adornData(ids, false)
        })
} 
export function saveOrUpdate(dataForm) {
    return http({
          url: http.adornUrl(`/form/data/${!dataForm.id ? 'save' : 'update'}`),
          method: 'post',
          data: http.adornData(dataForm)
        })
}
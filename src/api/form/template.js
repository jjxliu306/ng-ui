import http from '@/utils/httpRequest'


export function getList(params) {
  return http({
          url: http.adornUrl('/form/template/list'),
          method: 'get',
          params: params
        })
}


export function getInfo(id) {
  return http.get(http.adornUrl(`/form/template/info/${id}`))
}

export function getByCode(code) { 
 return http({
              url: http.adornUrl(`/form/template/queryByCode`),
              method: 'get',
              params: http.adornParams({
                code: code
              })
        })

}

/**
* 当前版本撤回（暂存版本）
*/
export function revoke(id) {
 return http({
          url: http.adornUrl('/form/template/revoke'),
          method: 'post',
          data: http.adornData({
            id: id
          })
        })
}

/**
* 发布（暂存版本）
*/
export function publish(id) {
 return http({
          url: http.adornUrl('/form/template/publish'),
          method: 'post',
          data: http.adornData({
            id: id
          })
        })
}


export function remove(ids) {
    return http({
          url: http.adornUrl('/form/template/delete'),
          method: 'post',
          data: http.adornData(ids, false)
        })
}

export function saveOrUpdate(dataForm) {
    return http({
          url: http.adornUrl(`/form/template/${!dataForm.id ? 'save' : 'update'}`),
          method: 'post',
          data: http.adornData(dataForm)
        })
}
import http from '@/utils/httpRequest'


export function getList(params) {
  	return http({
          url: http.adornUrl('/sys/sysdict/list'),
          method: 'get',
          params: params
        })
} 

export function getAll(){
    return http({
          url: http.adornUrl('/sys/sysdict/getAll'),
          method: 'get' 
        })
}

export function getTypes(params) {
  return  http({
        url: http.adornUrl("/sys/sysdict/types"),
        method: "get",
        params: http.adornParams(params)
      })
}

export function getByTypeValue(type , value) {
  return http({
          url: http.adornUrl(
            `/sys/sysdict/findByTypeValue?type=` + type + "&value=" + value
          ),
          method: "get",
          params: http.adornParams()
        })
}
 
/**
* 查询数据字典类型下的字典列表
*/
export function findByType(type) {
  return  http({
          url: http.adornUrl(
            `/sys/sysdict/findByType`
          ),
          method: "get",
          params: http.adornParams({
            type: type
          })
  })
}

export function deleteType(type) {
  return  http({
        url: http.adornUrl("/sys/sysdict/deleteType"),
        method: "post",
        data:  http.adornData(type, false)
      })
}
 
export function deleteDict(ids = []) {
  return http({
            url: http.adornUrl("/sys/sysdict/delete"),
            method: "post",
            data: http.adornData(ids, false)
          })
}

export function getById(id) {
	return http({
        url: http.adornUrl(`/sys/sysdict/info/${id}`),
        method: "get"
        })
}
 
export function updateType(data) {
	return http({
            url: http.adornUrl(`sys/sysdict/updateType`),
            method: "post",
            data: data
        })
}

export function saveOrUpdate(data) {
  return http({
            url: http.adornUrl(
              `/sys/sysdict/${!data.id ? "save" : "update"}`
            ),
            method: "post",
            data: data
        })
}
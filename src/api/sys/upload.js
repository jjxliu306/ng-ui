import http from '@/utils/httpRequest'

 
export function deleteFile(id) {
	return http({
            url: http.adornUrl(`/file/delete?id=${id}`),
            method: "get"
          })
}

export function getUploadUrl() {
  return http.adornUrl("/file/upload")
}

export function getDownUrl(id){
  return http.adornUrl(`/file/fileDown?uuid=${id}`) 
}

export function selectList(ids = []) {
  const ids_ = ids.join(',')
  return http({
        url: http.adornUrl(`/file/list?ids=` + ids_),
        method: "get",
        params: http.adornParams()
      })
}

 
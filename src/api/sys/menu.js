import http from '@/utils/httpRequest'


export function getList(params) {
  return http({
          url: http.adornUrl('/sys/menu/list'),
          method: 'get',
          params: params
        })
} 

export function selectList() {
  return  http({
        url: http.adornUrl("/sys/menu/select"),
        method: "get",
        params: http.adornParams()
      })
}

export function deleteMenu(id) {
	return http({
            url: http.adornUrl(`/sys/menu/delete/${id}`),
            method: "post" 
          }) 
}

export function getById(id) {
	return http({
        url: http.adornUrl(`/sys/menu/info/${id}`),
        method: "get"
        })
}
 
export function saveOrUpdate(data) {
	return http({
            url: http.adornUrl(
              `/sys/menu/${!data.menuId ? "save" : "update"}`
            ),
            method: "post",
            data: data
        })
}
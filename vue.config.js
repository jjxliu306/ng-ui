module.exports = {
	//去除生产环境的productionSourceMap
  productionSourceMap: false,
  chainWebpack: config => {
    config.plugin("html").tap(args => {
      args[0].title = "NG生态-ui";
      return args;
    });
  }
};

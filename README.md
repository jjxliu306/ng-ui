# ng-ui

## 简介
- ng-ui是基于vue、element-ui构建开发，实现[ng-server](https://gitee.com/jjxliu306/ng-server)后台管理前端功能，提供一套更优的前端解决方案
- 前后端分离，通过token进行数据交互，可独立部署
- 动态菜单，通过菜单管理统一管理访问路由
- 发布时，后端服务地址可以再配置文件中动态进行修改

**项目代码**
- ng-ui地址： https://gitee.com/jjxliu306/ng-ui
- ng-server地址：https://gitee.com/jjxliu306/ng-server
- 
**ng-form 动态表单系列**
- ng-form-element地址：https://gitee.com/jjxliu306/ng-form-element
- ng-form-iview地址：https://gitee.com/jjxliu306/ng-form-iview
- ng-form-elementplus地址：https://gitee.com/jjxliu306/ng-form-elementplus



## 项目结构
```
ng-ui
│
├─public 公共模块
│  ├─config 后端服务接口配置 
│
├─src 功能代码
   ├─api 接口封装
   ├─assets 资源目录，包含图片、样式、字体等
   ├─components 封装组件，目前包含 数据字典工具、上传组件等
   ├─element-ui element-ui按需加载
   ├─router 路由封装
   ├─store 状态管理
   ├─utils 常用js方法封装
   └─views	页面
        ├─common	公共页面，包含登录、404等	
        └─modules	各个功能模块页面
            ├─exter	三方对接api配置页面
            │  ├─app	三方对接app管理
            │  ├─log	三方调用日志
            │  └─perms	三方应用权限管理
            ├─form		ng-form动态表单示例
            │  ├─data	动态表单填报、处理
            │  └─template	动态表单模板
            ├─job		定时任务页面
            ├─main		空路由页面
            └─sys		系统管理页面
                ├─config	配置管理
                ├─dict		数据字典管理
                ├─menu 		菜单管理
                ├─role		角色管理
                └─user		用户管理
 

```
<br> 
 